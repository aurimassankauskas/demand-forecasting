# README #

This is a repository created for storing and versioning all the code used for demand forecasting model build and results extraction.

Please note that this repository became **irrelevant** when [Supply forecasting](https://bitbucket.org/aurimassankauskas/supply-forecasting/src/master/) repository was created. The latter is true because it covers both demand and supply forecasting as well as combined gap analysis.

### Main code files: ###

* **00_functions** - includes various self described functions that are used in the other notebooks.
* **01_data_load** - raw data load and preparation (i.e. filtering out irrelevant trusts, combining scarce trust data together, etc.) for the modelling.
* **02_main** - the main directory where all modelling and results extraction takes place. It is enough to run only this part of code, because 00_functions and 01_data_load are being sourced in here.
* **03_param_sel** - this file is used for parameter tuning, but can be considered as a irrelevant script if only the rerun of the model is needed, but no additional parameter tuning is done.
* **04_presentation_output** - additional ad hoc script used for extracting a few outputs for presentation creation. It is also irrelevant if only the rerun with additional data is needed.
