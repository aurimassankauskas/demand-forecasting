tmp = modelOutputs[[2]]
tmp = tmp[Trust == "East Kent Hospitals Uni NHS Foundation Trust" & StaffGroup != "Missing"]




dcast.data.table(tmp[smp == "val" & year < 2023], StaffGroup ~ year, fun.aggregate = sum, value.var = "fit")
dcast.data.table(tmp[smp == "val" & year < 2023], . ~ year, fun.aggregate = sum, value.var = "fit")[, !".", with = FALSE]




gr <- xgb.plot.tree(model=modelOutputs[[1]], trees=15, render=FALSE)
export_graph(gr, paste0(dir_output, 'tree.pdf'), width=150, height=600)





tmp = copy(ms4)
tmp = dcast.data.table(ms4, Trust + StaffGroup ~ smp, value.var = 'model 1', fun.aggregate = mean)


write.xlsx(tmp, paste0(dir_output, "02_MAPE_pres.xlsx"), sheetName = "good", row.names = FALSE)